# SendGrid

Package [@sendgrid/mail](https://www.npmjs.com/package/@sendgrid/mail) is used for only sending email.

To use all Sendgrid other , please use [@sendgrid/client](https://github.com/sendgrid/sendgrid-nodejs/tree/master/packages/client) which makes requests to [SendGrid v3 Web API](https://sendgrid.com/docs/API_Reference/api_v3.html)

There's possibility to use Web API and SMTP Relay. [Try more](https://app.sendgrid.com/guide/integrate)

---

### Transactional Templates

---

Creating templates is pretty straight forward and they can be easily added into sending email.

Twwo ways of creting templates:

#### 1. Create on the 'fly'

Create when sending email, need to add content property:

```
content: [
  {
      type: "text/html",
      value: "<h1 style='color:red'>test</h1>"
  }
],
```

#### 2. Create Transactional template

For using created templates in SendGrid you need to add template_id when sending mail.

For dynamic content SendGrid is using handlebars.js.

To set dynamic content of for example 'subject'.

- subject needs to be set as {{subject}} inside template
- dynamic_template_data must be send with key 'subject': value of

```
{
    dynamic_template_data: {
        subject: value
    }
}
```

#### Limitations

There are several rate limitations and restrictions that you should be aware of when using the v3 Mail Send endpoint.

- The total size of your email, including attachments, must be less than 30MB.
- The total number of recipients must no more than 1000. This includes all recipients defined within the to, cc, and bcc parameters, across each object that you include in the personalizations array.
- The total length of custom arguments must be less than 10000 bytes.
- Unicode encoding is not supported for the from field.
- The to.name, cc.name, and bcc.name personalizations cannot include either the ; or , characters.

#### Unsubscribe transactional email

[Issue in unsubscribe template design](https://github.com/sendgrid/sendgrid-ruby/issues/59#issuecomment-336284363)

Steps to add unsubscribe from transactional email:

#### 1. way

- Create Unsubscribe Group:
  - Suppresions -> Unsubscribe Groups
- Add asm group id in tranactional email send

```
asm: {
  group_id: 9059
}
```

#### 2. way

- add Subscription Tracking
- Settings -> Tracking -> Turn on
- Adds unsubscribe links to the bottom of the text and HTML emails. Future emails won't be delivered to unsubscribed users.

All unsubscribed emails are located inside Suppresions (Global Unsubscribes and Group Unsubscribes) based on which option have user used.

---

Activity tab shows all email history with lificycle messages

---

### Campaign

Campaign implements all from above inside it's created template

To create campaign sender (and confirm his email) and unsubscribe group has to be added:

From there it's pretty straightforward

- Go to Marketing -> Campaigns
- Create campaign
- Select from sender (it will prepulate footer sender details)
- Select receipients and add unsubscribe group (It will add link into Unsubscribe in footer)
- Build template with modules
- Send campaign
