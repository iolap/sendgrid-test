const client = require("@sendgrid/client");

client.setApiKey(process.env.SENDGRID_API_KEY);

client.setDefaultHeader(
  "Authorization",
  "Bearer " + process.env.SENDGRID_API_KEY
);

const request = {
  method: "POST"
};

exports.sendMail = (email, name) =>
  client
    .request({
      ...request,
      url: "/v3/mail/send",
      body: {
        personalizations: [
          {
            to: [
              {
                email: email,
                name: name || "John Doe"
              }
            ],
            dynamic_template_data: {
              subject: "Hi there,sent from SendGrid!"
            }
          }
        ],
        content: [
          {
            type: "text/html",
            value: "<h1 style='color:red'>test</h1>"
          }
        ],
        from: {
          email: "dpranjic@iolap.com",
          name: "iOLAP"
        },
        reply_to: {
          email: "dpranjic@iolap.com",
          name: "iOLAP"
        },
        template_id: "d-cc75a4af9cbd445486dda4f22006caec",
        asm: {
          group_id: 9059
        }
      }
    })
    .then(([response, body]) => {
      console.log(response.statusCode);
      console.log(body);
    });

exports.addContact = (email, name) =>
  client
    .request({
      ...request,
      url: "/v3/contactdb/recipients",
      body: [
        {
          email,
          first_name: name
        }
      ]
    })
    .then(([response, body]) => {
      console.log(response.statusCode);
      console.log(body);
    });

exports.getAllContactsList = async () => {
  return await client
    .request({
      method: "GET",
      url: "/v3/contactdb/recipients"
    })
    .then(([response, body]) => {
      // console.log(response.statusCode);
      // console.log(body);
      return body;
    });
};
