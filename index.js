const express = require("express");
const { addContact, sendMail, getAllContactsList } = require("./mail_helper");
const app = express();

const bodyParser = require("body-parser");

const cors = require("cors");

app.use(cors());
// parse application/json
app.use(bodyParser.json());
app.use(express.static("public"));

app.post("/send_mail", (req, res) => {
  const { email, name } = req.body;
  sendMail(email, name);
});

app.post("/add_contact", (req, res) => {
  const { email, name } = req.body;

  addContact(email, name);
});

app.post("/retrive_contacts", async (req, res) => {
  const t = await getAllContactsList();

  console.log(t);
  res.send(t);
});

app.listen(3000, () => {
  console.log("Started at http://localhost:3000");
});
